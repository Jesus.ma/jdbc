package utils;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.Cliente;


public class Pruebas {

    /*public static void main(String[] args) {
        String cadena_conexion="jdbc:mysql://localhost:3307/hr?useUnicode=true&serverTimezone=UTC";
        String usuario = "root";
        String contrasena = "1234";
        
        try{
            Connection conn = DriverManager.getConnection(cadena_conexion, usuario, contrasena);
            RegionController rc = new RegionController(conn);
            ResultSet rs = rc.selectAllRegions();
            
            while(rs.next()){
                int id = rs.getInt("region_id");
                String nombre = rs.getString("region_name");
                Region r1 = new Region(id, nombre);
                System.out.println(r1);
            }
            //cerrar recursos
            rs.close();
            conn.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        }
    }*/
   

    public static void main(String[]args){

        String cadena_conexion= "jdbc:mysql://localhost:3307/gimnasio?useUnicode=true&serverTimezone=UTC";
        String usuario = "root";
        String contrasena = "1234";

        try{
            Connection conn = new Conexion(cadena_conexion, usuario, contrasena).getConnection();
            //RegionController rc = new RegionController(conn);
            ResultSet rs;

            PreparedStatement stm = conn.prepareStatement("SELECT * FROM CLIENTE");
            rs = stm.executeQuery();



            while(rs.next()){
                int id = rs.getInt("idcliente");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                int edad = rs.getInt("edad");
                int idmaquinas = rs.getInt("idmaquinas");
                Cliente c1 = new Cliente(id, nombre, usuario, id, id);
                System.out.println(c1);
            }
            rs.close();
            conn.close();
            
        }catch (SQLException e){
            e.printStackTrace();
        }
        System.out.println();
    }
}

