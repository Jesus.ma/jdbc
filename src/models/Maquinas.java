package models;

public class Maquinas {
    
    private int idmaquinas;
    private String nombre;
    private int precio;
    private int kilogramos;

    public Maquinas(int idmaquinas, String nombre, int precio, int kilogramos) {
        this.idmaquinas = idmaquinas;
        this.nombre = nombre;
        this.precio = precio;
        this.kilogramos = kilogramos;
    }

    public int getIdmaquinas() {
        return idmaquinas;
    }

    public void setIdmaquinas(int idmaquinas) {
        this.idmaquinas = idmaquinas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getKilogramos() {
        return kilogramos;
    }

    public void setKilogramos(int kilogramos) {
        this.kilogramos = kilogramos;
    }

    @Override
    public String toString() {
        return "Maquinas{" + "idmaquinas=" + idmaquinas + ", nombre=" + nombre + ", precio=" + precio + ", kilogramos=" + kilogramos + '}';
    }
    
    
}
