package models;

import java.sql.ResultSet;

public interface EmpleadosDAO {
    
    public ResultSet selectAllEmpleados();
    public Empleados insertEmpleados(int idempleados, String nombre, String apellido, String puesto, int idcliente);
    public boolean deleteEmpleados(int idempleados);
    public Empleados updateEmpleados(int idempleados, String nombre, String apellido, String puesto, int idcliente);
    //extra
    public ResultSet selectEmpleadosByName(String nombre);
}
