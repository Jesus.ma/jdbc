package models;

public class Empleados {
    private int idempleados;
    private String nombre;
    private String apellido;
    private String puesto;
    private int idcliente;

    public Empleados(int idempleados, String nombre, String apellido, String puesto, int idcliente) {
        this.idempleados = idempleados;
        this.nombre = nombre;
        this.apellido = apellido;
        this.puesto = puesto;
        this.idcliente = idcliente;
    }

    public int getIdempleados() {
        return idempleados;
    }

    public void setIdempleados(int idempleados) {
        this.idempleados = idempleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    @Override
    public String toString() {
        return "Empleados{" + "idempleados=" + idempleados + ", nombre=" + nombre + ", apellido=" + apellido + ", puesto=" + puesto + ", idcliente=" + idcliente + '}';
    }
}
