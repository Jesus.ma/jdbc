package models;

import java.sql.ResultSet;

public interface ClienteDAO {
    
    public ResultSet selectAllCliente();
    public Cliente insertCliente(int idcliente, String nombre, String apellido, int edad, int idmaquinas);
    public boolean deleteCliente(int idcliente);
    public Cliente updateCliente(int idcliente, String nombre, String apellido, int edad, int idmaquinas);
    //extra
    public ResultSet selectClienteByName(String nombre);
}
