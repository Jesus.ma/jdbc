package models;

import java.sql.ResultSet;

public interface MaquinasDAO {
    
    public ResultSet selectAllMaquinas();
    public Maquinas insertMaquinas(int idmaquinas, String nombre, int precio, int kilogramos);
    public boolean deleteMaquinas(int idmaquinas);
    public Maquinas updateMaquinas(int idmaquinas, String nombre, int precio, int kilogramos);
    //extra
    public ResultSet selectMaquinasByName(String nombre);
}
