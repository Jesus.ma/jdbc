package models;

public class Cliente {
    private int idcliente;
    private String nombre;
    private String apellido;
    private int edad;
    private int idmaquinas;

    public Cliente(int idcliente, String nombre, String apellido, int edad, int idmaquinas) {
        this.idcliente = idcliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.idmaquinas = idmaquinas;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getIdmaquinas() {
        return idmaquinas;
    }

    public void setIdmaquinas(int idmaquinas) {
        this.idmaquinas = idmaquinas;
    }

    @Override
    public String toString() {
        return "cliente{" + "idcliente=" + idcliente + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", idmaquinas=" + idmaquinas + '}';
    }
}
